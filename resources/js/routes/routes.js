import PageError404 from "../components/pages/PageError404";
import {profile} from "./profile";
import PageLogin from "../components/pages/PageLogin";
import PageRegistration from "../components/pages/PageRegistration";

export const routes = [
    {path: '/login', component: PageLogin},
    {path: '/register', component: PageRegistration},
    profile,
    {path: '*', component: PageError404},
]
