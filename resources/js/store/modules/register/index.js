export const register = {
    state: {
        errors: [],
        name: '',
        surname: '',
        password: '',
        email: '',
        avatar: {
            file: null,
            url: ''
        }
    },
    mutations: {},
    actions: {},
    getters: {}
}
