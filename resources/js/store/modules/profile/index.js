export const profile = {
    state: {
        errors: [],
        user: {
            nameAndSurname: '',
            avatar: '',
            sex: '',
            birthday: ''
        },
        posts: [],
    },
    mutations: {},
    actions: {},
    getters: {}
}
