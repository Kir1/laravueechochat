function axiosRequest($this, url, data = null, actionName = '', requestStatusProperty = '') {
    return axios.post(url, data)
        .then(result => {
            $this.$store.commit(actionName, result.data);
            $this[requestStatusProperty] = 1;
        })
        .catch(errors => {
            $this[requestStatusProperty] = 2;
            throw errors;
        })
}

export function axiosRequestForComponent($this, url, data = null, actionName = '', requestStatusProperty = '', errorsProperty = '') {
    axiosRequest($this, url, data, actionName, requestStatusProperty)
        .catch(errors => {
            if (errors.response.data.errors instanceof Object)
                $this[errorsProperty] = {...errors.response.data.errors};
            else
                $this[errorsProperty] = {error: ['Упс! Ошибка на сервере.']};
        })
}

export function axiosRequestForState() {

}
