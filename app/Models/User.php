<?php

namespace App\Models;

class User
{
    protected $fillable = [
        'email', 'password', 'name', 'surname', 'avatar_micro', 'avatar_mini', 'avatar_middle', 'avatar_original'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
