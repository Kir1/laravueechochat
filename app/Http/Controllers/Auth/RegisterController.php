<?php

namespace App\Http\Controllers\Auth;

use App\Service\UserService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;

class RegisterController extends Controller
{
    public function register(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string|email|max:20|unique:users',
            'password' => 'required|string|min:6|max:20|confirmed',
            'name' => 'required|min:2|max:20',
            'surname' => 'required|min:2|max:20'
        ]);

        $user = UserService::createUserWithProfile(
            $request->email,
            Hash::make($request->password),
            $request->name,
            $request->surname,
            $request->avatarFile,
            $request->birthday,
            $request->sex
        )['user'];

        Auth::loginUsingId($user['id']);

        return UserService::formingUserMinifined($user);
    }
}
