<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;

class Profile extends Controller
{
    public static function get($message = 'Всё плохо')
    {
        return response()->json([
            'code' => 500,
            'message' => $message
        ], 500);
    }

    public static function update($message = 'Всё плохо')
    {
        return response()->json([
            'code' => 500,
            'message' => $message
        ], 500);
    }
}
