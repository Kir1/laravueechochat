<?php

namespace App\Services;

use App\Models\Profile;

class ProfileService
{
    const SEX_UNDEFINED = 0;
    const SEX_MALE = 1;
    const SEX_FEMALE = 2;

    public static function createProfile($user_id, $birthday, $sex)
    {
        $profile = ['user_id' => $user_id, 'birthday' => $birthday, 'sex' => $sex];
        Profile::create($profile);
        return $profile;
    }

    public static function getProfileForPage($user_id)
    {
        $profile = Profile::where('user_id', $user_id)->with('user')->first();
        if ($profile) {
            $profile = $profile->toArray();
            unset($profile['public']);
            $profile['nameAndSurname'] = $profile['user']['name'] . ' ' . $profile['user']['surname'];
            $profile['avatar'] = $profile['user']['avatar_middle'];
            unset($profile['user']);
        }
        return $profile;
    }
}
